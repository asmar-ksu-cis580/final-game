﻿namespace FinalGame.Model;

public enum BorderType
{
    None,
    Slimy,
    Sunny,
}