﻿using ColorMine.ColorSpaces;
using Microsoft.Xna.Framework;

namespace FinalGame.Model;

public struct Wall
{
    public Hsl Color;
    public Rectangle Rectangle;
}