﻿namespace FinalGame.Model;

public enum Border
{
    Right,
    Left,
    Top,
    Bottom,
}