﻿using ColorMine.ColorSpaces;

namespace FinalGame.Model;

public class WaypointColors
{
    public static readonly Hsl Marble = new Hsl
    {
        H = 266,
        S = 55 / 100.0,
        L = 50 / 100.0,
    };

    public static readonly Hsl Slimy = new Hsl()
    {
        H = 141,
        S = 55 / 100.0,
        L = 50 / 100.0,
    };

    public static readonly Hsl Sun = new Hsl()
    {
        H = 43,
        S = 55 / 100.0,
        L = 50 / 100.0,
    };
}