﻿using System;
using ColorMine.ColorSpaces;
using Microsoft.Xna.Framework;

namespace FinalGame.Model;

public struct Level
{
    public Func<GameTime, IColorSpace> BackgroundColorFunc;
    public bool HasSlimyBorder;
    public bool HasSunnyBorder;
    public string HelpText;
    public int NumberOfMoves;
    public Vector2 StarPosition;
    public Vector2 StartingPosition;
    public Wall[] Walls;
}