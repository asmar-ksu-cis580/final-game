﻿using FinalGame.Model;
using Microsoft.Xna.Framework;

namespace FinalGame.Repository;

public class LevelRepository
{
    private readonly Level[] _levels = new[]
    {
        new Level
        {
            BackgroundColorFunc = _ => WaypointColors.Slimy,
            HasSlimyBorder = false,
            HasSunnyBorder = false,
            HelpText =
                "Use the left mouse button to select\nthe next target. Remember, to hide this\nhelp, use the [H] key.",
            NumberOfMoves = 1,
            StarPosition = new Vector2(384, 540),
            StartingPosition = new Vector2(1536, 540),
            Walls = System.Array.Empty<Wall>(),
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Slimy,
            HasSlimyBorder = false,
            HasSunnyBorder = false,
            HelpText =
                "Look, you have more than\none move this turn.\nNotice that the puck follows your\nmouse cursor only until it hits the wall.",
            NumberOfMoves = 2,
            StarPosition = new Vector2(384, 540),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(584, 440, 40, 200)
                }
            }
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Sun,
            HasSlimyBorder = false,
            HasSunnyBorder = false,
            HelpText =
                "If you hit an obstacle after your last move is\nused, you will have to start over.\nThe coordinates in the top right will show you\nthe calculated targets.",
            NumberOfMoves = 2,
            StarPosition = new Vector2(84, 270),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(296, 112, 40, 640)
                },
            }
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Sun,
            HasSlimyBorder = false,
            HasSunnyBorder = false,
            HelpText =
                "Turns out that during planning, you live in\na parallel universe where walls don't exist.\nYour targets are always calculated from\nwhere you would've been without the walls.\nTry to first complete the level by aiming\nfor [144;  830] in the first move. Then use\n[Left] to return to this level. Then, try\naiming for [475;  936]. You can always\n[R]estart the level.",
            NumberOfMoves = 2,
            StarPosition = new Vector2(84 + 288, 270),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(296 + 288 - (296 - 72) - 40, 324, 40, 640)
                },
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(296 + 288, 112, 40, 500)
                },
            }
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Slimy,
            HasSlimyBorder = false,
            HasSunnyBorder = false,
            HelpText = "Use your newfound knowledge to\nmake your life easier.",
            NumberOfMoves = 3,
            StarPosition = new Vector2(640, 270),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(292, 864, 1536, 40)
                },
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(720, 100, 40, 540)
                },
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(540, 600, 220, 40)
                },
            }
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Marble,
            HasSlimyBorder = false,
            HasSunnyBorder = true,
            HelpText =
                "Look, a part of your puck has turned orange!\nTry using the [A] and [D] keys to rotate\nthe puck. When you hit a sunny wall with the\nsunny side, you bounce off of it without changing\nthe next target.",
            NumberOfMoves = 1,
            StarPosition = new Vector2(384, 540),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Slimy,
                    Rectangle = new Rectangle(584, 440, 40, 200)
                },
                new Wall()
                {
                    Color = WaypointColors.Sun,
                    Rectangle = new Rectangle(640, 940, 200, 40)
                }
            }
        },
        new Level()
        {
            BackgroundColorFunc = _ => WaypointColors.Slimy,
            HasSlimyBorder = false,
            HasSunnyBorder = true,
            HelpText = "Think carefully, you only have 2 moves.",
            NumberOfMoves = 2,
            StarPosition = new Vector2(640, 270),
            StartingPosition = new Vector2(1536, 540),
            Walls = new[]
            {
                new Wall()
                {
                    Color = WaypointColors.Sun,
                    Rectangle = new Rectangle(292, 864, 1536, 40)
                },
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(720, 100, 40, 540)
                },
                new Wall()
                {
                    Color = WaypointColors.Marble,
                    Rectangle = new Rectangle(540, 600, 220, 40)
                },
            }
        },
    };

    public Level[] GetLevels()
    {
        return _levels;
    }
}