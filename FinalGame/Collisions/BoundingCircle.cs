﻿using Microsoft.Xna.Framework;

namespace FinalGame.Collisions
{
    /// <summary>
    /// A struct representing circular bounds.
    /// </summary>
    public struct BoundingCircle
    {
        /// <summary>
        /// The center of the BoundingCircle.
        /// </summary>
        public Vector2 Center;

        /// <summary>
        /// The radius of the BoundingCircle.
        /// </summary>
        public float Radius;

        /// <summary>
        /// The left coordinate of the BoundingCircle.
        /// </summary>
        public float Left => Center.X - Radius;

        /// <summary>
        /// The right coordinate of the BoundingCircle.
        /// </summary>
        public float Right => Center.X + Radius;

        /// <summary>
        /// The top coordinate of the BoundingCircle.
        /// </summary>
        public float Top => Center.Y - Radius;

        /// <summary>
        /// The bottom coordinate of the BoundingCircle.
        /// </summary>
        public float Bottom => Center.Y + Radius;

        /// <summary>
        /// Constructs a new BoundingCircle.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="radius">The radius.</param>
        public BoundingCircle(Vector2 center, float radius)
        {
            Center = center;
            Radius = radius;
        }

        /// <summary>
        /// Tests for a collision between this and another bounding circle.
        /// </summary>
        /// <param name="other">The other bounding circle.</param>
        /// <returns>true if circles collide, false otherwise</returns>
        public bool CollidesWith(BoundingCircle other)
        {
            return CollisionHelper.Collide(this, other);
        }

        /// <summary>
        /// Tests for a collision between this and a bounding circle.
        /// </summary>
        /// <param name="other">The bounding circle.</param>
        /// <returns>true if they collide, false otherwise</returns>
        public bool CollidesWith(BoundingRectangle other)
        {
            return CollisionHelper.Collide(this, other);
        }
    }
}