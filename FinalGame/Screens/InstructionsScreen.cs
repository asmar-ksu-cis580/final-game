﻿using System;
using FinalGame.Model;
using FinalGame.StateManagement;
using FinalGame.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FinalGame.Screens;

public class InstructionsScreen : GameScreen
{
    private InputAction _continueAction = new InputAction(
        Array.Empty<Buttons>(),
        new[] { Keys.Space },
        true
    );

    private const string InstructionsDescription =
        @"Your goal is to reach the gray star. You can do that by bouncing off the walls.
On the top, you can see the number of moves you have left.
Select the direction in which you want to bounce off by using the left mouse button.
Use [A] and [D] to rotate the puck.
You can always [R]estart the current level, press [H] to hide help,
press [Left] to return to the previous level, or press [Right] to skip to the next level.
Press [Esc] to exit the game and [Backspace] to pause it.
To continue, press [Space].";

    public override void Draw(GameTime gameTime)
    {
        var spriteBatch = ScreenManager.SpriteBatch;
        var font = ScreenManager.Font;
        var viewport = ScreenManager.GraphicsDevice.Viewport;

        var measurement = font.MeasureString(InstructionsDescription);

        ScreenManager.GraphicsDevice.Clear(WaypointColors.Marble.ToColor());

        spriteBatch.Begin();
        spriteBatch.DrawString(
            font,
            InstructionsDescription,
            new Vector2(viewport.Width / 2f, viewport.Height / 2f) - measurement / 2f,
            Color.White
        );
        spriteBatch.End();
    }

    public override void HandleInput(GameTime gameTime, InputState input)
    {
        PlayerIndex playerIndex;
        if (_continueAction.Occurred(input, ControllingPlayer, out playerIndex))
        {
            ExitScreen();
            ScreenManager.AddScreen(new GameplayScreen(ScreenManager.Game), playerIndex);
        }
    }
}