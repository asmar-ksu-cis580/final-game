﻿using FinalGame.Entities;
using FinalGame.Model;
using Microsoft.Xna.Framework;
using FinalGame.StateManagement;
using FinalGame.Utils;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FinalGame.Screens
{
    // The main menu screen is the first thing displayed when the game starts up.
    public class MainMenuScreen : MenuScreen
    {
        private Texture2D _borderTexture;
        private ContentManager _content;
        private Puck _puck;

        public MainMenuScreen(Game game) : base(game, "Main Menu")
        {
            var playGameMenuEntry = new MenuEntry("Play Game");
            // var optionsMenuEntry = new MenuEntry("Options");
            var exitMenuEntry = new MenuEntry("Exit");

            playGameMenuEntry.Selected += PlayGameMenuEntrySelected;
            // optionsMenuEntry.Selected += OptionsMenuEntrySelected;
            exitMenuEntry.Selected += OnCancel;

            MenuEntries.Add(playGameMenuEntry);
            // MenuEntries.Add(optionsMenuEntry);
            MenuEntries.Add(exitMenuEntry);

            _puck = new Puck(RandomHelper.RandomDirection(-MathHelper.Pi, MathHelper.Pi), new Vector2(1536, 540));
        }

        public override void Activate()
        {
            _content ??= new ContentManager(ScreenManager.Game.Services, "Content");
            _borderTexture = _content.Load<Texture2D>("border");
            _puck.LoadContent(_content);
        }

        private void PlayGameMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new InstructionsScreen());
        }

        private void OptionsMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new OptionsMenuScreen(_game), e.PlayerIndex);
        }

        protected override void OnCancel(PlayerIndex playerIndex)
        {
            const string message = "Are you sure you want to exit this sample?";
            var confirmExitMessageBox = new MessageBoxScreen(message);

            confirmExitMessageBox.Accepted += ConfirmExitMessageBoxAccepted;

            ScreenManager.AddScreen(confirmExitMessageBox, playerIndex);
        }

        private void ConfirmExitMessageBoxAccepted(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.Game.Exit();
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            var viewport = ScreenManager.GraphicsDevice.Viewport;

            if (_puck.Bounds.Top < Puck.RenderResolution / 2f ||
                _puck.Bounds.Bottom > viewport.Height - Puck.RenderResolution / 2f)
            {
                _puck.Center -= _puck.Direction * 10f;
                _puck.Direction = new Vector2(_puck.Direction.X, -_puck.Direction.Y);
            }
            else if (_puck.Bounds.Left < Puck.RenderResolution / 2f ||
                     _puck.Bounds.Right > viewport.Width - Puck.RenderResolution / 2f)
            {
                _puck.Center -= _puck.Direction * 10f;
                _puck.Direction = new Vector2(-_puck.Direction.X, _puck.Direction.Y);
            }

            var updateContext = new UpdateContext()
            {
                GraphicsDevice = ScreenManager.GraphicsDevice,
                GameTime = gameTime,
            };
            _puck.Update(updateContext);

            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);
        }

        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(WaypointColors.Marble.ToColor());

            var spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(_borderTexture, Vector2.Zero, Color.White);
            var drawingContext = new DrawingContext
            {
                GraphicsDevice = ScreenManager.GraphicsDevice,
                SpriteBatch = spriteBatch
            };
            _puck.Draw(drawingContext);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}