﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ColorMine.ColorSpaces;
using FinalGame.Collisions;
using FinalGame.Entities;
using FinalGame.Model;
using FinalGame.Repository;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using FinalGame.StateManagement;
using FinalGame.Utils;

namespace FinalGame.Screens
{
    // This screen implements the actual game logic. It is just a
    // placeholder to get the idea across: you'll probably want to
    // put some more interesting gameplay in here!
    public class GameplayScreen : GameScreen
    {
        private Game _game;

        private ContentManager _content;
        private SpriteFont _gameFont;

        private float _pauseAlpha;
        private readonly InputAction _pauseAction;

        private readonly InputAction _nextLevelAction;
        private readonly InputAction _previousLevelAction;
        private readonly InputAction _restartAction;
        private readonly InputAction _helpAction;

        private Puck _puck;

        private Texture2D _arrowTexture;
        private float _arrowScale;
        private const float ArrowRenderHeight = ArrowRenderWidth * 253f / 573f;
        private const float ArrowRenderWidth = Puck.RenderResolution;
        private Vector2 _arrowDirection;
        private const float ArrowGap = 0f;

        private Texture2D _starTexture;
        private float _starScale;
        private const float StarRenderWidth = Puck.RenderResolution;

        private const float RotationCoefficient = MathHelper.PiOver2 * 3f;

        private Texture2D _borderTexture;

        private enum GameState
        {
            Choosing,
            Playing,
        }

        private Level[] _levels;
        private int _currentLevel;
        private int _remainingMoves;
        private List<Vector2> _targets;
        private int _currentTarget;
        private int _nextTarget;
        private GameState _gameState = GameState.Choosing;
        private bool _leftMouseDown = false;
        private bool _showHelp = true;

        public GameplayScreen(Game game)
        {
            _game = game;

            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            _pauseAction = new InputAction(
                new[] { Buttons.Start, Buttons.Back },
                new[] { Keys.Back, Keys.Escape }, true
            );

            _nextLevelAction = new InputAction(
                Array.Empty<Buttons>(),
                new[] { Keys.Right }, true
            );
            _previousLevelAction = new InputAction(
                Array.Empty<Buttons>(),
                new[] { Keys.Left }, true
            );

            _restartAction = new InputAction(
                Array.Empty<Buttons>(),
                new[] { Keys.R }, true
            );

            _helpAction = new InputAction(
                Array.Empty<Buttons>(),
                new[] { Keys.H }, true
            );

            _levels = new LevelRepository().GetLevels();

            _puck = new Puck(Vector2.Zero, Vector2.Zero);

            SetupCurrentLevel();
        }

        // Load graphics content for the game
        public override void Activate()
        {
            if (_content == null)
                _content = new ContentManager(ScreenManager.Game.Services, "Content");

            _gameFont = _content.Load<SpriteFont>("gamefont");

            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

            _puck.LoadContent(_content);

            _starTexture = _content.Load<Texture2D>("star");
            _starScale = StarRenderWidth / _starTexture.Width;

            _arrowTexture = _content.Load<Texture2D>("arrow");
            _arrowScale = ArrowRenderWidth / _arrowTexture.Width;

            _borderTexture = _content.Load<Texture2D>("border");

            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }


        public override void Deactivate()
        {
            base.Deactivate();
        }

        public override void Unload()
        {
            _content.Unload();
        }

        // This method checks the GameScreen.IsActive property, so the game will
        // stop updating when the pause menu is active, or if you tab away to a different application.
        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                _pauseAlpha = Math.Min(_pauseAlpha + 1f / 32, 1);
            else
                _pauseAlpha = Math.Max(_pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                var mouseState = Mouse.GetState();
                var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

                var updateContext = new UpdateContext
                {
                    GameTime = gameTime,
                    GraphicsDevice = ScreenManager.Graphics.GraphicsDevice,
                };

                _puck.Update(updateContext);

                if (_gameState == GameState.Choosing)
                {
                    _arrowDirection = new Vector2(mouseState.X, mouseState.Y) - _puck.Center;
                }
            }
        }

        // Unlike the Update method, this will only be called when the gameplay screen is active.
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            // Look up inputs for the active player profile.
            var playerIndex = (int)ControllingPlayer.Value;

            var keyboardState = input.CurrentKeyboardStates[playerIndex];
            var gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected && input.GamePadWasConnected[playerIndex];

            PlayerIndex player;
            if (_pauseAction.Occurred(input, ControllingPlayer, out player) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(_game), ControllingPlayer);
            }
            else
            {
            }

            if (_nextLevelAction.Occurred(input, ControllingPlayer, out player))
            {
                _currentLevel += 1;
                if (_currentLevel == _levels.Length)
                {
                    _currentLevel = 0;
                }

                SetupCurrentLevel();
            }

            if (_previousLevelAction.Occurred(input, ControllingPlayer, out player))
            {
                _currentLevel -= 1;
                if (_currentLevel == -1)
                {
                    _currentLevel = _levels.Length - 1;
                }

                SetupCurrentLevel();
            }

            if (_restartAction.Occurred(input, ControllingPlayer, out player))
            {
                SetupCurrentLevel();
            }

            if (_helpAction.Occurred(input, ControllingPlayer, out player))
            {
                _showHelp = !_showHelp;
            }

            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

            if (_gameState == GameState.Choosing)
            {
                var mouseState = Mouse.GetState();
                if (_leftMouseDown == false && mouseState.LeftButton == ButtonState.Pressed)
                {
                    _leftMouseDown = true;
                    _remainingMoves -= 1;

                    //_puck.Center.X + _arrowDirection.X * N == Puck.RenderResolution / 2f;
                    //_puck.Center.X + _arrowDirection.X * N == viewport.Width - Puck.RenderResolution / 2f;

                    if (_arrowDirection.X == 0)
                    {
                        // go figure
                        _arrowDirection.X = 0.01f;
                    }

                    if (_arrowDirection.Y == 0)
                    {
                        _arrowDirection.Y = 0.01f;
                    }

                    var fieldBox = new Rectangle(
                        new Point((int)Puck.RenderResolution / 2),
                        new Point(
                            (int)(viewport.Width - Puck.RenderResolution),
                            (int)(viewport.Height - Puck.RenderResolution)
                        )
                    );
                    var puckBox = _puck.Rectangle;

                    (float xFieldBoundary, float xPuckBoundary) = _arrowDirection.X < 0
                        ? (fieldBox.Left, puckBox.Left)
                        : (fieldBox.Right, puckBox.Right);
                    (float yFieldBoundary, float yPuckBoundary) = _arrowDirection.Y < 0
                        ? (fieldBox.Top, puckBox.Top)
                        : (fieldBox.Bottom, puckBox.Bottom);
                    float xDirectionalCoefficient = (xFieldBoundary - xPuckBoundary) / _arrowDirection.X;
                    float yDirectionalCoefficient = (yFieldBoundary - yPuckBoundary) / _arrowDirection.Y;
                    float directionalCoefficient = Math.Min(xDirectionalCoefficient, yDirectionalCoefficient);
                    var translatedCenter = _puck.Center + directionalCoefficient * _arrowDirection;
                    _targets.Add(translatedCenter);
                    _puck.Center = translatedCenter;

                    if (_remainingMoves == 0)
                    {
                        _currentTarget = -1;
                        _nextTarget = 0;
                        _puck.Center = _levels[_currentLevel].StartingPosition;
                        _gameState = GameState.Playing;
                    }
                }
                else if (mouseState.LeftButton == ButtonState.Released)
                {
                    _leftMouseDown = false;
                }
            }
            else if (_gameState == GameState.Playing)
            {
                if (keyboardState.IsKeyDown(Keys.A))
                {
                    _puck.PuckRotation += RotationCoefficient * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (keyboardState.IsKeyDown(Keys.D))
                {
                    _puck.PuckRotation -= RotationCoefficient * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (_currentTarget == -1)
                {
                    _puck.Center = _levels[_currentLevel].StartingPosition;
                }

                if (_currentTarget < _nextTarget && _nextTarget < _targets.Count)
                {
                    _puck.Direction = _targets[_nextTarget] - _puck.Center;
                    _puck.Direction = Vector2.Normalize(_puck.Direction);
                    _currentTarget = _nextTarget;
                }

                if (_puck.Bounds.CollidesWith(new BoundingCircle(_levels[_currentLevel].StarPosition,
                        StarRenderWidth / 2f)))
                {
                    _currentLevel += 1;
                    if (_currentLevel == _levels.Length)
                    {
                        _currentLevel = 0;
                    }

                    SetupCurrentLevel();
                }
                else if (_puck.Bounds.Top < Puck.RenderResolution / 2f ||
                         _puck.Bounds.Bottom > viewport.Height - Puck.RenderResolution / 2f ||
                         _puck.Bounds.Left < Puck.RenderResolution / 2f ||
                         _puck.Bounds.Right > viewport.Width - Puck.RenderResolution / 2f)
                {
                    PushPuckOutOfBorder();
                    _puck.Direction = Vector2.Zero;
                    _nextTarget += 1;
                    if (_nextTarget == _targets.Count)
                    {
                        SetupCurrentLevel();
                    }
                }
                else
                {
                    foreach (var wall in _levels[_currentLevel].Walls)
                    {
                        if (_puck.Bounds.CollidesWith(new BoundingRectangle(wall.Rectangle)))
                        {
                            _puck.Center -= _puck.Direction * 10f;
                            (var closestPointOnRectangle, bool isCorner) =
                                wall.Rectangle.ClosestPointTo(_puck.Center.ToPoint());
                            var directionalVector = closestPointOnRectangle.ToVector2() - _puck.Center;
                            directionalVector.Normalize();
                            var collisionAngle = (float)Math.Atan2(-directionalVector.Y, directionalVector.X);
                            var collidedBorderType = _puck.BorderTypeAtAngle(collisionAngle);

                            var puckBox = _puck.Rectangle;
                            var borderByDistance = new (Border, float)[]
                            {
                                (Border.Right, closestPointOnRectangle.X - puckBox.Right),
                                (Border.Left, closestPointOnRectangle.X - puckBox.Left),
                                (Border.Top, closestPointOnRectangle.Y - puckBox.Top),
                                (Border.Bottom, closestPointOnRectangle.Y - puckBox.Bottom),
                            };
                            var collidedBorder = borderByDistance.MinBy(t => Math.Abs(t.Item2)).Item1;

                            if (collidedBorderType == BorderType.None)
                            {
                                _nextTarget += 1;
                                _puck.Direction = Vector2.Zero;
                                if (_nextTarget == _targets.Count)
                                {
                                    SetupCurrentLevel();
                                }
                            }
                            else if (collidedBorderType == BorderType.Slimy)
                            {
                                _nextTarget += 1;
                                _puck.Direction = Vector2.Zero;
                                if (_nextTarget == _targets.Count)
                                {
                                    SetupCurrentLevel();
                                }
                            }
                            else if (collidedBorderType == BorderType.Sunny)
                            {
                                if (wall.Color != WaypointColors.Sun)
                                {
                                    _nextTarget += 1;
                                    _puck.Direction = Vector2.Zero;
                                    if (_nextTarget == _targets.Count)
                                    {
                                        SetupCurrentLevel();
                                    }
                                }
                                else if (isCorner)
                                {
                                    _puck.Direction = -directionalVector;
                                }
                                else
                                {
                                    switch (collidedBorder)
                                    {
                                        case Border.Right:
                                        case Border.Left:
                                            _puck.Direction = new Vector2(-_puck.Direction.X,
                                                _puck.Direction.Y);
                                            break;
                                        case Border.Top:
                                        case Border.Bottom:
                                            _puck.Direction = new Vector2(_puck.Direction.X,
                                                -_puck.Direction.Y);
                                            break;
                                        default:
                                            throw new ArgumentOutOfRangeException();
                                    }
                                }
                            }

                            break;
                        }
                    }
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            // Our player and enemy are both actually just text strings.
            var spriteBatch = ScreenManager.SpriteBatch;

            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;

            var drawingContext = new DrawingContext
            {
                SpriteBatch = spriteBatch,
                GraphicsDevice = ScreenManager.Graphics.GraphicsDevice,
            };

            ScreenManager.GraphicsDevice.Clear(_levels[_currentLevel].BackgroundColorFunc(gameTime).ToColor());

            spriteBatch.Begin();

            foreach (var wall in _levels[_currentLevel].Walls)
            {
                var wallTexture = new Texture2D(ScreenManager.Graphics.GraphicsDevice, 1, 1);
                wallTexture.SetData(new[] { wall.Color.ToColor() });
                spriteBatch.Draw(wallTexture, wall.Rectangle, Color.White);
            }

            var starDimensions = new Vector2(_starTexture.Width, _starTexture.Height) * _starScale;
            spriteBatch.Draw(
                _starTexture,
                _levels[_currentLevel].StarPosition - starDimensions / 2f,
                null,
                Color.White,
                0f,
                Vector2.Zero,
                _starScale,
                SpriteEffects.None,
                0
            );

            _puck.Draw(drawingContext);

            if (_gameState == GameState.Choosing)
            {
                spriteBatch.Draw(
                    _arrowTexture,
                    _puck.Center,
                    null,
                    Color.White,
                    (float)Math.Atan2(_arrowDirection.Y, _arrowDirection.X),
                    new Vector2(_arrowTexture.Width / 2f, _arrowTexture.Height / 2f),
                    _arrowScale,
                    SpriteEffects.None,
                    0
                );

                var levelText = $"Level: {_currentLevel}";
                var levelTextDimensions = _gameFont.MeasureString(levelText);
                spriteBatch.DrawString(
                    _gameFont,
                    levelText,
                    new Vector2((viewport.Width - levelTextDimensions.X) / 2f, 80),
                    Color.White
                );

                var movesText = $"Moves: {_remainingMoves}";
                var movesTextDimensions = _gameFont.MeasureString(movesText);
                spriteBatch.DrawString(
                    _gameFont,
                    movesText,
                    new Vector2((viewport.Width - movesTextDimensions.X) / 2f, 100 + levelTextDimensions.Y),
                    Color.White
                );

                if (_showHelp)
                {
                    string helpText = _levels[_currentLevel].HelpText;
                    var helpTextDimensions = _gameFont.MeasureString(helpText);
                    spriteBatch.DrawString(
                        _gameFont,
                        helpText,
                        new Vector2(
                            viewport.Width - helpTextDimensions.X - Puck.RenderResolution / 2f - 40,
                            100
                        ),
                        Color.White
                    );
                }
            }
            else if (_gameState == GameState.Playing)
            {
                var targetString = "";
                for (var i = 0; i < _targets.Count; i++)
                {
                    var target = _targets[i];
                    targetString += $"[{(int)target.X};  {(int)target.Y}]";
                    if (i == _currentTarget)
                    {
                        targetString += "  *";
                    }

                    targetString += "\n";
                }

                spriteBatch.DrawString(
                    _gameFont,
                    targetString,
                    new Vector2(1536, 100),
                    Color.White
                );
            }

            spriteBatch.Draw(_borderTexture, Vector2.Zero, Color.White);

            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || _pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, _pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }

        private void SetupCurrentLevel()
        {
            var level = _levels[_currentLevel];
            _remainingMoves = level.NumberOfMoves;
            _targets = new List<Vector2>();
            _puck.IsSlimy = level.HasSlimyBorder;
            _puck.IsSunny = level.HasSunnyBorder;
            _puck.PuckRotation = 0f;
            _puck.ShadowRotation = 0f;
            _puck.Center = level.StartingPosition;
            _puck.Direction = Vector2.Zero;
            _gameState = GameState.Choosing;
        }

        private void PushPuckOutOfBorder()
        {
            var viewport = ScreenManager.Graphics.GraphicsDevice.Viewport;
            _puck.Center = new Vector2(
                Math.Clamp(
                    _puck.Center.X,
                    Puck.RenderResolution,
                    viewport.Width - Puck.RenderResolution
                ),
                Math.Clamp(
                    _puck.Center.Y,
                    Puck.RenderResolution,
                    viewport.Height - Puck.RenderResolution
                )
            );
        }
    }
}