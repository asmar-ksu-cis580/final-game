﻿using Microsoft.Xna.Framework;

namespace FinalGame.Screens
{
    // The options screen is brought up over the top of the main menu
    // screen, and gives the user a chance to configure the game
    // in various hopefully useful ways.
    public class OptionsMenuScreen : MenuScreen
    {
        public OptionsMenuScreen(Game game) : base(game, "Options")
        {
            SetMenuEntryText();

            var back = new MenuEntry("Back");
            back.Selected += OnCancel;
            MenuEntries.Add(back);
        }

        // Fills in the latest values for the options screen menu text.
        private void SetMenuEntryText()
        {
        }
    }
}