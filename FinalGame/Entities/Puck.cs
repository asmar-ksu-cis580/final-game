﻿using System;
using System.Diagnostics;
using System.Linq;
using FinalGame.Collisions;
using FinalGame.Model;
using FinalGame.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FinalGame.Entities
{
    public class Puck
    {
        private Texture2D _baseTexture;
        private Texture2D _shadowTexture;
        private Texture2D _sunBorderTexture;
        private Texture2D _slimeBorderTexture;

        public bool IsSunny = false;
        public bool IsSlimy = false;

        public const float BorderRadialLength = MathHelper.TwoPi / 3f;
        public const float SlimyBorderStart = MathHelper.PiOver2 - BorderRadialLength;
        public const float SunnyBorderStart = MathHelper.PiOver2;

        private Vector2 _direction;

        public Vector2 Direction
        {
            get => _direction;
            set
            {
                _direction = value;
                ShadowRotation = (float)Math.Atan2(value.Y, value.X);
            }
        }

        private Vector2 _center;

        public Vector2 Center
        {
            get => _center;
            set
            {
                _center = value;
                Bounds.Center = value;
            }
        }

        public Rectangle Rectangle =>
            new Rectangle(
                (Center - new Vector2(RenderResolution / 2f)).ToPoint(),
                new Point((int)RenderResolution)
            );

        private float _puckRotation = 0f;

        public float PuckRotation
        {
            get => _puckRotation;
            set => _puckRotation = MathHelper.WrapAngle(value);
        }

        public float ShadowRotation = 0f;

        private float _scale;
        public const float RenderResolution = 144f;

        public BoundingCircle Bounds;

        public Puck(Vector2 direction, Vector2 center)
        {
            Direction = direction;
            Center = center;
            Bounds = new BoundingCircle(center, RenderResolution / 2);
        }

        public void LoadContent(ContentManager contentManager)
        {
            _baseTexture = contentManager.Load<Texture2D>("puck_base");
            _shadowTexture = contentManager.Load<Texture2D>("puck_shadow");
            _sunBorderTexture = contentManager.Load<Texture2D>("puck_sun_border");
            _slimeBorderTexture = contentManager.Load<Texture2D>("puck_slime_border");
            _scale = RenderResolution / _baseTexture.Width;
        }

        public void Update(UpdateContext updateContext)
        {
            var viewport = updateContext.GraphicsDevice.Viewport;

            var totalSeconds = (float)updateContext.GameTime.ElapsedGameTime.TotalSeconds;

            float axisDelta = totalSeconds * 350;
            {
                Center += axisDelta * Direction;
                float x = MathHelper.Clamp(Center.X, RenderResolution / 2, viewport.Width - RenderResolution / 2);
                float y = MathHelper.Clamp(Center.Y, RenderResolution / 2, viewport.Height - RenderResolution / 2);
                Center = new Vector2(x, y);
            }
        }

        public void Draw(DrawingContext context)
        {
            var origin = new Vector2((float)_baseTexture.Width / 2, (float)_baseTexture.Height / 2);
            context.SpriteBatch.Draw(
                _baseTexture,
                Center,
                null,
                Color.White,
                0f,
                origin,
                _scale,
                SpriteEffects.None,
                0
            );

            if (IsSunny)
            {
                context.SpriteBatch.Draw(
                    _sunBorderTexture,
                    Center,
                    null,
                    Color.White,
                    -PuckRotation,
                    origin,
                    _scale,
                    SpriteEffects.None,
                    0
                );
            }

            if (IsSlimy)
            {
                context.SpriteBatch.Draw(
                    _slimeBorderTexture,
                    Center,
                    null,
                    Color.White,
                    -PuckRotation,
                    origin,
                    _scale,
                    SpriteEffects.None,
                    0
                );
            }

            context.SpriteBatch.Draw(
                _shadowTexture,
                Center,
                null,
                Color.White,
                ShadowRotation,
                origin,
                _scale,
                SpriteEffects.None,
                0
            );
        }

        public BorderType BorderTypeAtAngle(float angle)
        {
            Debug.WriteLine(MathHelper.ToDegrees(angle));
            Debug.WriteLine(MathHelper.ToDegrees(MathHelper.WrapAngle(SunnyBorderStart + PuckRotation)));
            Debug.WriteLine(
                MathHelper.ToDegrees(MathHelper.WrapAngle(SunnyBorderStart + BorderRadialLength + PuckRotation)));

            (float, float, BorderType)[] borders =
            {
                (SunnyBorderStart, SunnyBorderStart + BorderRadialLength, BorderType.Sunny),
            };
            var rotatedBorders = borders.Select(
                ((float borderStart, float borderEnd, BorderType borderType) t) => (
                    MathHelper.WrapAngle(t.borderStart + PuckRotation),
                    MathHelper.WrapAngle(t.borderEnd + PuckRotation),
                    t.borderType
                )
            );

            foreach ((float borderStart, float borderEnd, var borderType) in rotatedBorders)
            {
                if (borderStart < borderEnd)
                {
                    if (angle > borderStart && angle < borderEnd)
                        return borderType;
                }
                else if (angle > borderStart || angle < borderEnd)
                {
                    return borderType;
                }
            }

            return BorderType.None;
        }
    }
}