﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FinalGame.Utils;

public struct UpdateContext
{
    public GameTime GameTime { get; init; }
    public GraphicsDevice GraphicsDevice { get; init; }
}