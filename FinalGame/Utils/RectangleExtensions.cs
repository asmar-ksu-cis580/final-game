﻿using Microsoft.Xna.Framework;

namespace FinalGame.Utils;

public static class RectangleExtensions
{
    public static (Point point, bool isCorner) ClosestPointTo(this Rectangle rectangle, Point point)
    {
        var retPoint = new Point(
            rectangle.Left > point.X ? rectangle.Left : (rectangle.Right < point.X ? rectangle.Right : point.X),
            rectangle.Top > point.Y ? rectangle.Top : (rectangle.Bottom < point.Y ? rectangle.Bottom : point.Y)
        );
        bool isCorner = (retPoint.X == rectangle.Left || retPoint.X == rectangle.Right) &&
                        (retPoint.Y == rectangle.Top || retPoint.Y == rectangle.Bottom);
        return (retPoint, isCorner);
    }
}