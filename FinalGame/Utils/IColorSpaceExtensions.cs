﻿using ColorMine.ColorSpaces;
using Microsoft.Xna.Framework;

namespace FinalGame.Utils;

public static class ColorSpaceExtensions
{
    public static Color ToColor(this IColorSpace color)
    {
        var rgb = color.To<Rgb>();
        return new Color((int)rgb.R, (int)rgb.G, (int)rgb.B);
    }
}