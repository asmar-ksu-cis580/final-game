﻿using Microsoft.Xna.Framework.Graphics;

namespace FinalGame.Utils
{
    public struct DrawingContext
    {
        public SpriteBatch SpriteBatch { get; init; }
        
        public GraphicsDevice GraphicsDevice { get; init; }
    }
}